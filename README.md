# Ejemplo de uso de semaforos

## Caso base
* Tenemos un [recurso compartido](./src/dev/dmartinez/models/Recurso.java) al que vamos a acceder desde varios hilos para extraer la información.
* Este recurso tiene un método `utilizar` que elimina y devuelve el último número del array si este no está vacio.
* Tenemos una clase [Consumidor](./src/dev/dmartinez/models/Consumidor.java) que usará el recurso compartido hasta que este le devuelva un null.
* En el ejecutador llenamos el recurso y creamos los hilos que lo van a consumir.

## Problema existente
Al utilizar el recurso fuera de una sección crítica y por tanto no ser una operación atómica, nos encontramos con un problema de inconsistencia de memoria, que se traduce en la excepcion `ArrayIndexOutOfBoundsException`.

## Soluciones planteadas
Esto se resuelve convirtiendo el acceso a recurso en una operación atómica, para ello se requiere usar métodos de sincronización de hilos.

### Semaforo MUTEX
Utilizar un semáforo de exclusión mutua para controlar los accesos al recurso.
Se puede encontrar el ejemplo en la rama `sync/semaphore_mutex`

### Monitor de recurso
Se utiliza un monitor sobre el recurso. Se puede encontrar el ejemplo en la rama `sync/monitor`
