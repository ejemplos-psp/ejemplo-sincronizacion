package dev.dmartinez;

import dev.dmartinez.models.Consumidor;
import dev.dmartinez.models.Recurso;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class Executor {

	private final static int CANTIDAD = 20;
	private final static int HILOS = 5;

	public static void main(String[] args) throws InterruptedException {
		ArrayList<Thread> hilos = new ArrayList<>();
		ArrayList<String> datos = new ArrayList<>();

		Recurso recurso = new Recurso(datos);

		for (int i = 0; i < HILOS; i++) {
			hilos.add(new Consumidor(recurso));
		}

		for (int i = 0; i < CANTIDAD; i++) {
			datos.add(String.valueOf(i));
		}

		for (Thread hilo : hilos) {
			hilo.start();
		}
	}
}
