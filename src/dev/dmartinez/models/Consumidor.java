package dev.dmartinez.models;

public class Consumidor extends Thread {
	private Recurso recurso;

	public Consumidor(Recurso recurso) {
		this.recurso = recurso;
	}

	@Override
	public void run() {
		String dato = null;
		do {
			dato = recurso.utilizar();
			if (dato != null) {
				System.out.println(getName() + ": " + dato);
			}
		} while (dato != null);
	}
}
