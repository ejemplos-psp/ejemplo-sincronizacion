package dev.dmartinez.models;

import java.util.ArrayList;

public class Recurso {
	private ArrayList<String> datos;

	public Recurso(ArrayList<String> datos) {
		this.datos = datos;
	}

	public String utilizar() {
		String dato = null;
		if (datos.size() > 0) {
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			dato = datos.remove(datos.size() - 1);
		}
		return dato;
	}
}
